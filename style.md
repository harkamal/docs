# Style Guide

Since Ruby doesn't really have statements, in this text "statements" refers to expressions that usually are statements (except for expression statements) in other languages, including multiline blocks. `if`, `while`, `do/begin`-`end` and so on.

## Names

* All names should be as descriptive as possible. For loop counters shorter names are acceptable. These can be commonly used single letters like `i`, `j` and `e`
* Do not shorten names, there are a few exceptions to this when the shortened name is commonly know and ok to use, like shortening `information` to `info`
* Do not use abbreviations, except for when the abbreviation is more commonly known then what it actually stands for. Examples of this are: `HTTP`, `FTP` and `WWW`

### Case conventions:

#### Camel Case

First letter is always a lower case letter. No separation between words and  each new word starts with an upper case letter. For abbreviations the case of the first letter dictates the rest of the casing of the abbreviation. Examples are `fooBar`, `foo`, `httpConnection`, `connectWithHTTP`.

http://en.wikipedia.org/wiki/CamelCase

#### Pascal Case

Pascal case is similar to camel case but the first letter is always an upper case letter. Examples are `FooBar`, `Foo`, `HTTPConnection`, `ConnectWithHTTP`.

http://en.wikipedia.org/wiki/CamelCase#Variations_and_synonyms

#### Snake Case

Snake case always start with a lower case letter. Each new word is separated with an underscore. Examples are: `foo_bar`, `foo`, `http_connection`, `connect_with_http`.

http://en.wikipedia.org/wiki/Snake_case

### Variables

All variable names use [snake case](#snake-case), except for constants. Example:

```ruby
foo = 3
foo_bar = 4
@x = 4
@foo_bar_baz = "asd"
```

### Methods

All method names use the [snake case](#snake-case) naming convention. Example:

```ruby
def foo
end

def foo_bar
end

def foo_bar=
end

def foo_bar?
end
```

### Classes and Modules

Classes and modules follow the same naming convention. They use the [pascal case](#pascal-case) naming convention. Example:

```ruby
class Foo
end

class FooBar
end

module HTTP
end

class HTTPConnection
end
```

### Constants

Constants use the [snake case](#snake-case) naming convention but with all letters in upper case.

```ruby
class Foo
  FOO_BAR = 3
  BAR = 4
end
```

## Indentation

Two spaces are used for indentation. When a line needs to continue to the next line it's indented with one level (two spaces).

```ruby
def foo
  "foo"
end

class Bar
  def foo
    "Bar.foo"
  end


  class NestedClass
    def bar
    end

    protected

    def foo
    end
  end

  private

  def bar
    if @a == 0
      "@a == 0"
    else
      "@a != 0"
    end
  end

  def x
    [1, 2, 3].each do |e|
      p e
    end
  end
end

module Foo
  class FooBar
    def foo_bar
      [1, 2, 3, 4].map{ |e| e * 2}.
        select{ |e| e > @some_variable }.
        join("-")
    end
  end
end
```

### Case Statements

* `when`-statements and the `else` part are indented one level
* The contents of the `else` and `when`-statements are indented one level as well
* The `else` and `when`-statements are always indented to the same level

```ruby
case a
  when 3 then "3"
  when 4 then "4"
  when 5 then "5"

  when 6
    foo
    bar
  when 7
    bar
    foo
  else
    foo_bar
end
```

## Spacing and Newlines

Prefer to code in a spacious style. Always have an empty newline between method, module and class declarations.

### Operators

Always use one space before and after binary operators. Never use a space after  unary operators. Example:

```ruby
a + 3 # correct
a+3 # wrong

a[1 .. 2] # correct
a[1 .. -1] # correct

a[1..2] # wrong
a[1..-1] # wrong

a ? b : c # correct
a?b:c # wrong
```

### Methods

* Never use a space before the opening parenthesis for methods with a parameter list
* Always use a space after the comma in the parameter list
* Methods are always separated with an empty newline

```ruby
def foo
end

def foo (a)
end

def foo (a, b)
end
```

### Classes and Modules

* Always use a space before and after the less then sign `<` when declaring a subclass.
* Always use a space before and after the double less then sign `<<` when opening a singleton class
* Classes and modules are always separated with an empty newline

```ruby
class Foo
end

class Base
  class << self
  end
end

class Sub < Base
end

module Namespace
  class Bar
  end

  class Foo
    def foo
    end

    def bar
    end
  end
end

module Mod
end
```

### Statements

* Prefer to separate statements with an empty newline from other statements and expressions

```ruby
a = 3

if a < 3
  a = 2
end

a = 4
b = [1, 2, 3]

b.each do |e|
  p e
end

def foo
  a = 3

  begin
    bar
  rescue Exception => e
    handle_exception(e)
  ensure
    delete_bar
  end

  puts "Logging bar"
end

while a < 3
  a += 1
end

a = 4
a = [1, 2, 3, 4].map{ |e| e * 3 }
a = a.select{ |e| e < 5 }
```

### If Statements

If statements are an important part of most imperative languages and also with Ruby. While they should be used to handle the control flow, it is important to not over use if-statements as this will most probably end up in "if-else hell". This is not a place you want to be and neither should you want your colleagues to end up there.

* If a predicate contains more than 2 conditions it is good practice to move the predicate into a method detailing what the predicate is actually trying to achieve.
* Always keep the shortest path on top, if they are equal the happy path goes first.
* Never define a variable in the predicate as this might lead to headaches to figure out scope later down the road.


```ruby
# More than 2 conditions

# wrong
def foo_method
  if foo == bar && bar == baz && baz == bax
    ...
  end
end

# right
def foo_method
  if foo_is_equal_to_bax
    ...
  end
end

def foo_is_equal_to_bax
  foo == bar && bar == baz && baz == bax
end

# Structuring

# wrong
if !foo
  sad_path
else
  happy_path
end

# right
if foo
  happy_path
else
  sad_path
end

# wrong
if !foo
  sad_path
  ...
  ...
  ...
  ...
else
  happy_path
  ...
end

# right
if foo
  happy_path
  ...
else
  sad_path
  ...
  ...
  ...
  ...
end

# Defining variables used in if statements

# wrong
if foo = Bar.find_by_id(x)
  foo.bar_method
end

# right
foo = Bar.find_by_id(x)

if foo
  foo.bar_method
end
```

### Unless Statements
While unless isn't called if, it is the reverse of the same and as such should be used where applicable.

* NEVER EVER write `unless not foo` or `unless !foo` this is what you have `if` for!
* NEVER use `else` with `unless`, use `if`.

### Case Statements
Case statements follow the same rules as [statements](#statements) except what's stated here.

* Use one-liner `when`-statements if the expression is short
* `when`-statements are _never_ separated with an empty newline
* The `case`-statement itself is separated with an empty newline as usual
* The contents of the `else` part can be put on the same line as the `else` itself, if it's short enough. Same rules as for the first point

```ruby
case a
  when 3 then "3"
  when 4 then "4"
  when 5 then "5"

  when 6
    foo
    bar
  when 7
    bar
    foo
  else
    foo_bar
end
```

### Expressions

* Expressions are usually not separated with an empty newline
* Expressions are allowed to be separated to group several expressions together
* Expressions are also allowed to be separated if there are many expressions after each other. This should preferably never happen. It's usually an indication that the method is too long and should be split in to multiple smaller methods

```ruby
a = 3
b = 4
c = 5

rect = Rect.new
rect.x += 1
rect.y += 1
rect.width += 2
rect.height += 2

perform(rect)

if rect.x < 10
  rect.y += rect.x
end

action(rect)
```

### Variables

* Variables are always declared/initialized on its own line
* The only exception to this rule is when a function returns multiple values
* Never align variable declarations
* Prefer to put variable declarations with longer names last in a group of declarations, unless this hurts readability or grouping

```ruby
a = 3
b = 4

result, error = action(a, b)

# correct

foo = 4
bar = 5
foo_bar = 3

# wrong
foo     = 4
foo_bar = 3
bar     = 5
```

### Hash Literals

* Always use a space after the opening brace and a space before the closing brace.
* Always use a space after the comma
* Always use a space before and after the hash rockets `=>`
* Always use a space after the colon in the symbol name when using Ruby 1.9 style hash literals
* Use one space between the opening and closing brace for empty hash literals

```ruby
a = { foo: 3, bar: 4 }
b = { "foo" => 3, "bar" => 4 }
c = { }

foo(foo: 3, bar: 4)
bar({ foo: 3, bar: 4 })
action foo: 3, bar: 4
```

### Block Literals

#### Brace Style

* Always use a space after the opening brace and a space before the closing brace.
* Always use a space before and after the parameter list
* Use one space between the opening and closing brace for empty block literals
* Always use one space after the method name and before the opening brace

```ruby
a = [1, 2, 3, 4].select { |e| e < 4 }
b = lambda { }
```

#### Do-End Style

* Always use a space before the parameter list

```ruby
[1, 2, 3].each do |e|
  p e
end
```

### Line Continuation

Stay width in 80 columns wide lines. Avoid line continuation if possible. If
you must use it:

* Break the line where it naturally fits
* Try to only use the line continuation symbol `\` for strings
* If possible, always break with a symbol that indicates the line continuation on the next line for the parser to make sense, even if not strictly required by the language. This mean, try to end the line with a comma or other symbol that a line can't end with. Except for a dot which is placed on the next line

```ruby
a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].select{ |number| number < 4 }
  .map{ |number| format_number(number) }

# even though the language doesn't require this line to end with a comma, since
# it's between parentheses, prefer to end it with a comma anyway
Namespace::Mod::HTTPConnection.new.connect_with_numbers(1, 2, 3, 4, 5,
  6, 7, 8, 9)
```

## Parentheses

* Avoid unnecessary parentheses

```ruby
# correct
if a < b
end

# wrong
if (a < b)
end
```

### Methods

* Do not use parentheses for methods without parameters
* Always use parentheses for the parameter list

```ruby
# correct
def foo
end

# wrong
def foo()
end

# correct
def bar(a, b)
end

# wrong
def bar a, b
end
```

## Languages Constructs

### Hash Literals

* Always prefer 1.9 style hash literals if possible
* Avoid using the braces if possible

```ruby
foo(a: 1, b: 2) # correct
foo("a" => 1, "b" => 2) # correct
foo({a: 1, b: 2}) # wrong
foo({:a => 1, :b => 2}) # wrong
```

### Block Literals


#### Brace Style

* Only use this style for one-liners
* Only a single expression is allowed in the block contents
* Prefer this style if doesn't clutter or make the code less readable
* Do not use this style if the block is passed, inline, to a method and the method is acting like a statement

```ruby
loop { } # wrong
before(:each) { get :new } #wrong
let(:foo) { create(:foo) }
```

#### Do-End Style

* Never use this style for one-liners
* Always use this style if the block is passed, inline, to a method and the method is acting like a statement

```ruby
# correct
loop do
end

# correct
before :each do
  get :new
end

let(:bar) do
  item = create(:bar)
  item.x = 3
  item
end
```